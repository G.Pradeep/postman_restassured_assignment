package Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetMethodSingleUser {
	@Test(priority = 1)
	public void getMethod() {
		RestAssured.baseURI = "https://reqres.in";
		Response get = given().baseUri(baseURI).when().get("/api/users/2").then().statusCode(200).extract().response();
		String res = get.asString();
		System.out.println(res);
		JsonPath js = new JsonPath(res);
		System.out.println(js.getString("data.last_name"));
		Assert.assertEquals(js.getString("data.last_name").equalsIgnoreCase("Weaver"), true);
	}
}
