package Assignment;

import org.json.simple.JSONObject;
import io.restassured.parsing.Parser;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class GoRestRequests {
	String accessToken ="Bearer a032ff2b81e1e8ab1f8fc6e2907f88646f16c30ec2c2499fc630615530f174b7";
	String id = "";
	JSONObject req = new JSONObject();
	String baseURI = "https://gorest.co.in";
	@Test(priority = 2)
	public void getMethod() {
		String get =  given().baseUri(baseURI).header("Authorization",accessToken).when().get("/public/v2/users/"+ id).then().extract().response().asString();
		System.out.println(get);
	}

	@Test(priority = 1)
	public void postMethod() {
		req.put("name", "pradeep");
		req.put("email", "pradeepg123@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		String post = given().baseUri(baseURI).headers("Authorization",accessToken,"Content-Type","application/json")
				.body(req.toJSONString())
				.when().post("/public/v2/users")
				.then().extract().response().asString();
		JsonPath jsp = new JsonPath(post);
		id = jsp.getString("id");
		System.out.println(post);
		req.clear();
	}

	@Test(priority = 3)
	public void putMethod() {
		req.put("name", "pradeep");
		req.put("email", "pradeepg1234@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		
		String put = given().baseUri(baseURI).header("Authorization",accessToken)
				.body(req.toJSONString())
				.when().put("/public/v2/users/"+id)
				.then().extract().response().asString();
		System.out.println(put);
		req.clear();
	}

	@Test(priority = 4)
	public void deleteMethod() {
		given().baseUri(baseURI).header("Authorization",accessToken).when().delete("/public/v2/users/"+id).then().extract().response().asString();
	}
}
